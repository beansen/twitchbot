package data;

import java.util.ArrayList;

/**
 * Created by beansen on 12.06.17.
 */
public class YahtzeeData extends AbstractGameData {

	private ArrayList<YahtzeePlayerData> players;

	private int[] currentRoll;

	private int currentPlayer;

	public YahtzeeData() {
		players = new ArrayList<>();
		currentRoll = new int[5];
	}

	@Override
	public boolean isPlaying(String name) {
		for (int i = 0; i < players.size(); i++) {
			if (players.get(i).getName().equals(name)) {
				return true;
			}
		}
		return false;
	}

	@Override
	public void startNewGame() {
		players.clear();
		currentPlayer = 0;
	}

	@Override
	public void addPlayer(String name) {
		players.add(new YahtzeePlayerData(name));
	}

	public boolean isPlayersTurn(String name) {
		for (int i = 0; i < players.size(); i++) {
			if (players.get(i).getName().equals(name) && i == currentPlayer) {
				return true;
			}
		}
		return false;
	}

	public void setCurrentRoll(int index, int number) {
		currentRoll[index] = number;
	}

	public void sortDice() {
		int temp;
		for (int i = 1; i < currentRoll.length; i++) {
			for (int j = 0; j < currentRoll.length - 1; j++) {
				if (currentRoll[j] > currentRoll[j + 1]) {
					temp = currentRoll[j];
					currentRoll[j] = currentRoll[j + 1];
					currentRoll[j + 1] = temp;
				}
			}
		}
	}

	public int getPlayerCount() {
		return players.size();
	}

	public YahtzeePlayerData getPlayerData(int index) {
		return players.get(index);
	}

	public class YahtzeePlayerData {

		private String name;
		private int[] sectionScores;

		public YahtzeePlayerData(String name) {
			this.name = name;
			sectionScores = new int[13];

			for (int i = 0; i < sectionScores.length; i++) {
				sectionScores[i] = -1;
			}
		}

		public String getName() {
			return name;
		}
	}
}
