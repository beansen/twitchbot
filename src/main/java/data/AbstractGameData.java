package data;

import java.util.ArrayList;

/**
 * Created by beansen on 11.06.17.
 */
public abstract class AbstractGameData {


	abstract public boolean isPlaying(String name);
	abstract public void startNewGame();
	abstract public void addPlayer(String name);
}
