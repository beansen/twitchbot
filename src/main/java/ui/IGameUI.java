package ui;

import javafx.scene.layout.StackPane;

/**
 * Created by beansen on 12.06.17.
 */
public interface IGameUI<F> {

	void updateUI(int index, F data);
	StackPane getLayout();
}
