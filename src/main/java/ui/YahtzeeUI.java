package ui;

import data.YahtzeeData;
import javafx.scene.layout.StackPane;

/**
 * Created by beansen on 12.06.17.
 */
public class YahtzeeUI implements IGameUI<YahtzeeData.YahtzeePlayerData> {

	private StackPane root;

	public YahtzeeUI() {
		root = new StackPane();
	}

	@Override
	public void updateUI(int index, YahtzeeData.YahtzeePlayerData data) {

	}

	@Override
	public StackPane getLayout() {
		return root;
	}

	public void updateDiceUI(int[] dice) {

	}
}
