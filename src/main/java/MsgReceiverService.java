import javafx.concurrent.Service;
import javafx.concurrent.Task;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

/**
 * Created by beansen on 11.06.17.
 */
public class MsgReceiverService extends Service<String> {

    private SocketClient socketClient;

    public MsgReceiverService(SocketClient socketClient) {
        this.socketClient = socketClient;
    }

    @Override
    protected Task<String> createTask() {
        return new Task<String>() {
            @Override
            protected String call() throws Exception {
                return socketClient.receiveData();
            }
        };
    }
}
