import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;

/**
 * Created by beansen on 09.06.17.
 */
public class CommandsHandler {

    private HashMap<String, CommandResponse> responses;
    private SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");
    private Date date;

    public CommandsHandler() {
        date = new Date();
        responses = new HashMap<>();

        responses.put("!hello", new CommandResponse() {
            @Override
            public String getResponse() {
                return "Well hello my friend!";
            }
        });

        responses.put("!time", new CommandResponse() {
            @Override
            public String getResponse() {
                date.setTime(System.currentTimeMillis());
                return String.format("Beansen's current time is: %s", dateFormat.format(date));
            }
        });
    }

    public String getResponse(String msg) {
        for (String key : responses.keySet()) {
            if (msg.contains(key)) {
                return responses.get(key).getResponse();
            }
        }

        return null;
    }
}
