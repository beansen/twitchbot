/**
 * Created by beansen on 09.06.17.
 */
public interface CommandResponse {

    String getResponse();
}
