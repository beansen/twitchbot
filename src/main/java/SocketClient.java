
import com.oracle.javafx.jmx.json.JSONDocument;
import com.oracle.javafx.jmx.json.JSONFactory;
import com.oracle.javafx.jmx.json.JSONReader;

import java.io.*;
import java.net.Socket;

/**
 * Created by beansen on 09.06.17.
 */
public class SocketClient {

    private Socket socket;
    private PrintWriter printWriter;
    private BufferedReader bufferedReader;
    private String password;
    private String identity;
    private String channel;

    public SocketClient(String path) {
        try {
            FileReader fileReader = new FileReader(path + "preferences.json");
            JSONReader reader = JSONFactory.instance().makeReader(fileReader);
	        JSONDocument document = reader.build();
            password = (String) document.object().get("password");
            identity = (String) document.object().get("identity");
            channel = (String) document.object().get("channel");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    public void connect(String host, int port) {
        try {
            socket = new Socket(host, port);
            printWriter = new PrintWriter(socket.getOutputStream());
            bufferedReader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public boolean isConnected() {
        return socket != null && socket.isConnected();
    }

    public void init() {
        printWriter.append(String.format("PASS %s\r\n", password));
        printWriter.append(String.format("NICK %s\r\n", identity));
        printWriter.append(String.format("JOIN %s\r\n", channel));
        printWriter.flush();
    }

    public void sendMessage(String message) {
        printWriter.append(String.format("PRIVMSG %1s :%2s\r\n", channel, message));
        printWriter.flush();
    }

    public String receiveData() {
        try {
            String msg = bufferedReader.readLine();
            if (msg.contains("PRIVMSG")) {
                return msg;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }

    public void close() {
        try {
            //bufferedReader.close();
            //printWriter.close();
            socket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
