import javafx.application.Application;
import javafx.concurrent.WorkerStateEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import logic.YahtzeeLogic;

/**
 * Created by beansen on 09.06.17.
 */
public class Main extends Application {

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        primaryStage.setTitle("Twitch Bot");
        Parameters parameters = getParameters();

        if (parameters.getUnnamed().size() > 0) {
	        CommandsHandler commandsHandler = new CommandsHandler();
	        SocketClient client = new SocketClient(parameters.getUnnamed().get(0));
	        client.connect("irc.chat.twitch.tv", 6667);

	        if (client.isConnected()) {
		        client.init();

		        MsgSenderService msgSenderService = new MsgSenderService(client);
		        MsgReceiverService msgReceiverService = new MsgReceiverService(client);

		        msgReceiverService.setOnSucceeded(new EventHandler<WorkerStateEvent>() {
			        @Override
			        public void handle(WorkerStateEvent event) {
				        System.out.println(event.getSource().getValue());
				        String msg = (String) event.getSource().getValue();

				        if (msg != null) {
					        String resp = commandsHandler.getResponse(msg);

					        if (resp != null) {
						        msgSenderService.setMessage(resp);
						        msgSenderService.reset();
						        msgSenderService.start();
					        }
				        }

				        msgReceiverService.reset();
				        msgReceiverService.start();
			        }
		        });

		        msgReceiverService.setOnCancelled(new EventHandler<WorkerStateEvent>() {
			        @Override
			        public void handle(WorkerStateEvent event) {
				        System.out.println("Service canceled");
			        }
		        });

		        msgReceiverService.start();
	        }

	        primaryStage.setOnCloseRequest(new EventHandler<WindowEvent>() {
		        @Override
		        public void handle(WindowEvent event) {
			        System.out.println("Close app");
		        }
	        });

	        YahtzeeLogic yahtzeeLogic = new YahtzeeLogic();
	        primaryStage.setScene(new Scene(yahtzeeLogic.getUi().getLayout(), 1280, 720));
        }

	    primaryStage.show();
    }
}
