package logic;

import com.oracle.tools.packager.JreUtils;
import data.YahtzeeData;
import ui.YahtzeeUI;

import java.util.HashMap;
import java.util.Random;
import java.util.regex.Pattern;

/**
 * Created by beansen on 11.06.17.
 */
public class YahtzeeLogic extends AbstractGameLogic {


	private YahtzeeData data;
	private YahtzeeUI ui;
	private Random random;
	private HashMap<String, RuleCheck> rulesCheckMap;

	private boolean gameRunning;

	public YahtzeeLogic() {
		data = new YahtzeeData();
		ui = new YahtzeeUI();
		random = new Random();
		initRules();
	}

	@Override
	public void handleInput(String msg) {
		String[] split = msg.split("(!)|(:)");
		String player = split[1];
		String message = split[split.length - 1];

		if (data.isPlaying(player) && data.isPlayersTurn(player)) {
			if (rulesCheckMap.containsKey(message.toLowerCase())) {

			}
		} else {
			if (!gameRunning && message.equals(code)) {
				data.addPlayer(player);
				ui.updateUI(data.getPlayerCount() - 1, data.getPlayerData(data.getPlayerCount() - 1));
			}
		}
	}

	public YahtzeeUI getUi() {
		return ui;
	}

	private void rollDice(int[] keep) {
		int index = 0;

		for (int i = 0; i < 5; i++) {
			if (keep != null && i == keep[index]) {
				index++;
			} else {
				data.setCurrentRoll(i, random.nextInt(6) + 1);
			}
		}

		data.sortDice();
	}

	private void initRules() {
		rulesCheckMap = new HashMap<>(13, 1);

		//Upper section
		rulesCheckMap.put("aces", new UpperSectionRuleCheck(1));
		rulesCheckMap.put("twos", new UpperSectionRuleCheck(2));
		rulesCheckMap.put("threes", new UpperSectionRuleCheck(3));
		rulesCheckMap.put("fours", new UpperSectionRuleCheck(4));
		rulesCheckMap.put("fives", new UpperSectionRuleCheck(5));
		rulesCheckMap.put("sixes", new UpperSectionRuleCheck(6));

		//Lower section
		rulesCheckMap.put("three of a kind", new MultipleOfKindRuleCheck(3));
		rulesCheckMap.put("four of a kind", new MultipleOfKindRuleCheck(4));
		rulesCheckMap.put("full house", new FullHouseRuleCheck());
		rulesCheckMap.put("small straight", new StraightRuleCheck(4, 30));
		rulesCheckMap.put("large straight", new StraightRuleCheck(5, 40));
		rulesCheckMap.put("yahtzee", new YahtzeeRuleCheck());
		rulesCheckMap.put("chance", new ChanceRuleCheck());
	}

	private interface RuleCheck {
		boolean hasScored(int[] currentRole);
		int getScore();
	}

	private class UpperSectionRuleCheck implements RuleCheck {

		private int score;
		private int number;

		public UpperSectionRuleCheck(int number) {
			this.number = number;
		}

		@Override
		public boolean hasScored(int[] currentRole) {
			score = 0;

			for (int i = 0; i < currentRole.length; i++) {
				if (currentRole[i] == number) {
					score += number;
				}
			}

			return score > 0;
		}

		@Override
		public int getScore() {
			return score;
		}
	}

	private class MultipleOfKindRuleCheck implements RuleCheck {

		private int score;
		private int amount;

		public MultipleOfKindRuleCheck(int amount) {
			this.amount = amount;
		}

		@Override
		public boolean hasScored(int[] currentRole) {
			score = 0;
			int found = 0;

			for (int i = 0; i < currentRole.length; i++) {
				score += currentRole[i];

				if (i < currentRole.length - 1) {
					found += currentRole[i] == currentRole[i + 1] ? 1 : 0;
				}
			}

			return found >= amount - 1;
		}

		@Override
		public int getScore() {
			return score;
		}
	}

	private class FullHouseRuleCheck implements RuleCheck {

		@Override
		public boolean hasScored(int[] currentRole) {
			int found = 0;

			for (int i = 0; i < currentRole.length - 1; i++) {
				found += currentRole[i] == currentRole[i + 1] ? 1 : 0;
			}

			return found == 3;
		}

		@Override
		public int getScore() {
			return 25;
		}
	}

	private class StraightRuleCheck implements RuleCheck {

		private int amount;
		private int score;

		public StraightRuleCheck(int amount, int score) {
			this.amount = amount;
			this.score = score;
		}

		@Override
		public boolean hasScored(int[] currentRole) {
			int found = 0;

			for (int i = 0; i < currentRole.length - 1; i++) {
				if (currentRole[i] + 1 == currentRole[i + 1]) {
					found++;
				} else if (found < amount) {
					found = 0;
				}
			}

			return found >= amount - 1;
		}

		@Override
		public int getScore() {
			return score;
		}
	}

	private class YahtzeeRuleCheck implements RuleCheck {

		@Override
		public boolean hasScored(int[] currentRole) {
			for (int i = 0; i < currentRole.length - 1; i++) {
				if (currentRole[i] != currentRole[i + 1]) {
					return false;
				}
			}
			return true;
		}

		@Override
		public int getScore() {
			return 50;
		}
	}

	private class ChanceRuleCheck implements RuleCheck {

		private int score;

		@Override
		public boolean hasScored(int[] currentRole) {
			score = 0;
			for (int i = 0; i < currentRole.length; i++) {
				score += currentRole[i];
			}
			return true;
		}

		@Override
		public int getScore() {
			return score;
		}
	}
}
