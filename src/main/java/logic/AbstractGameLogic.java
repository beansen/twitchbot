package logic;

/**
 * Created by beansen on 11.06.17.
 */
public abstract class AbstractGameLogic {

    protected String code = "testCode";
    abstract public void handleInput(String msg);
}
