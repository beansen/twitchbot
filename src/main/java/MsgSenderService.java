import javafx.concurrent.Service;
import javafx.concurrent.Task;

/**
 * Created by beansen on 17.06.17.
 */
public class MsgSenderService extends Service {

	private SocketClient socketClient;

	private String msg;

	public MsgSenderService(SocketClient socketClient) {
		this.socketClient = socketClient;
	}

	public void setMessage(String msg) {
		this.msg = msg;
	}
	@Override
	protected Task createTask() {
		return new Task() {
			@Override
			protected Object call() throws Exception {
				socketClient.sendMessage(msg);
				return null;
			}
		};
	}
}
